# Hands-on

Here are some excercises to practice the new skills.

For the hands-on we will use the [yes, no, maybe api](https://yesno.wtf/api)

## Client
Create a client with the following requirements (take small steps):
1. Create a command-line tool to consume this api, and it should only return yes, no or maybe.
1. Build the tool and try to cross compile
1. Add some flags to add extra functionality:
    - make the number of requests configurable
    - make boolean to count all yes, no and maybes
    - make boolean to return the answer and gif urls seperated by a tab

## Server
Create a server which accepts a get request with a query parmeter of the amount of queries that should be done. Let it render a HTML table with the answer and the gif rendered side-by-side.





